<?php
/**
 * All callbacks for the batch processing 
 * of taxonomy deletions
 */


/**
 * The main batch operations callback that
 * does the deleting.
 */
function delete_all_taxonomies_batch_delete($tid_chunks, $term_count, &$context) {

    // Load all taxonomies under the vocabulary
    if (!isset($context['sandbox']['progress'])) {
        
        // Fetch the number of rows left to process
        $context['sandbox']['max'] = $term_count;
        $context['sandbox']['progress'] = 0;
    }

    foreach ($tid_chunks as $chunk) {

        $result = taxonomy_term_delete($chunk);

        if ($result !== SAVED_DELETED) {
            $context['errors'][] = $chunk . ' failed to deleted.';
        }

        $context['results'][] = $chunk;
        $context['message'] = t('Deleting term %tid', array('%tid' => $chunk));
        $context['sandbox']['progress']++;
    }

    if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
        $context['finished'] = 1;
    } 

}


/**
 * Executed at the end of the deletion process
 */
function delete_all_taxonomies_delete_by_vid_finished($success, $results, $operations) {
  drupal_set_message(t('Done!'), 'status');
}
